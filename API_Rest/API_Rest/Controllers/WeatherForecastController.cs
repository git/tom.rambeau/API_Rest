using Microsoft.AspNetCore.Mvc;
using System;

namespace API_Rest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static List<String> Summaries = new List<String>
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public static List<WeatherForecast> Weathers = new List<WeatherForecast>
        {

           new WeatherForecast
            {
               Id = 1,
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(Random.Shared.Next(0,5))),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Count)]
            },
           new WeatherForecast
           {

               Id = 2,
               Date = DateOnly.FromDateTime(DateTime.Now.AddDays(Random.Shared.Next(0,5))),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Count)]
           },
           new WeatherForecast
           {

               Id = 3,
               Date = DateOnly.FromDateTime(DateTime.Now.AddDays(Random.Shared.Next(0,5))),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Count)]
           }
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]

        [Route("/api/WeatherForecast/getAll/")]
        public IEnumerable<WeatherForecast> GetAll()
        {
            return Weathers
            .ToArray();

        }

        [HttpGet]
        [Route("/api/WeatherForecast/getOne/{id}")]
        
        public async Task<ActionResult<WeatherForecast>> GetOne(long id)
        {

            foreach (WeatherForecast wf in Weathers)
            {
                if (wf.Id == id)
                {
                    return Ok(wf);
                }
            }
            return BadRequest();

        }

        [HttpPost(Name = "PostWeatherForecast")]
        public async Task<ActionResult<WeatherForecast>> Post(WeatherForecast wf)
        {
            //_context.TodoItems.Add(todoItem);
            //await _context.SaveChangesAsync();
           Weathers.Add(wf);
           return CreatedAtAction(nameof(Created), new { date = wf.Date }, wf);
        }

        [HttpDelete]
        [Route("/api/WeatherForecast/delete/{id}")]
        public async Task<ActionResult> Delete(long id)
        {
            foreach (WeatherForecast wf in Weathers)
            {
                if (wf.Id == id)
                {
                    Weathers.Remove(wf);
                    return Ok();
                }
            }
            return BadRequest();


        }
    }
}
